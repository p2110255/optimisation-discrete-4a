Plusieurs exemples de programmes a lancer sont présents dans le fichier Main.java

//*********************************************************************
Chaque partie de code est commentée et possède un titre qui délimite la partie à décommenter pour la tester.

Pour décommenter, enlever les éléments "/*" et "*/" respectivement au début et à la fin de la partie à tester.

Une fois une partie testée, il faut la recommenter avant d'en lancer une autre.


//************************************************************************
La fonction getFile("dataXXX") permet de recupérer les données d'un jeu de données précis. 

Tous les jeux de données sont utilisables sur tous les codes commentés.

Pour changer de jeu de données, il suffit d'indiquer l'id de celui dans le paramètre en entrée de la fonction getFile (exemple getFile("data1202");).


//************************************************************************
La fonction output() précédée d'une solution (exemple sol.output()) récupère toutes les routes et la fitness d'une solution 
et les inscrits dans le fichier Data/output.txt.

Sont affichés dans ce fichier : id du client/ readyTime / servedTime / dueTime / Demand 

Attention : cette fonction écrit à la suite du fichier texte. Il faut parfois veiller à vider le fichier output.txt avant de réaliser un test 
pour être certain du résultat souhaité.
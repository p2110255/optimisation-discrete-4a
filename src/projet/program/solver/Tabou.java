package projet.program.solver;

import projet.program.models.Neighbor;
import projet.program.models.Permutation;
import projet.program.models.Solution;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Tabou {
    public ArrayList<Permutation> permutationInterdites = new ArrayList<>();
    public HashMap<Solution, ArrayList<Permutation>> etatsVisites = new HashMap<>();

    public Solution initialSolution;

    public final int maxIterations;
    public final int maxTabouSize;

    //Constructor
    public Tabou(int maxIterations, int maxTabouSize, Solution solution) {
        this.maxIterations = maxIterations;
        this.maxTabouSize = maxTabouSize;
        this.initialSolution = solution;
    }

    public Object[] solve(){
        Solution currentSol = new Solution(initialSolution);
        Solution bestSol = new Solution(initialSolution);
        bestSol.recalculate();
        int nbIterations = 0;

        for(int i = 0; i < maxIterations; i++){
            Neighbor bestNeighbour = new Neighbor();
            ArrayList<Neighbor> neighbourhoud = bestNeighbour.getNeighbours(currentSol);
            if(neighbourhoud.size()==0){
                break;
            }
            //On enlève les solutions présentent dans la liste tabou
            for(int k = 0; k<neighbourhoud.size(); k++){
                Neighbor currentNeighbor = neighbourhoud.get(k);
                nbIterations += currentNeighbor.permutationDone.nbIteration;
                if(permutationInterdites.contains(currentNeighbor.permutationDone)){
                    //System.out.println("elementdelete");
                    neighbourhoud.remove(currentNeighbor);
                }
            }
            //On récupère le meilleur voisin
            for(Neighbor currentNeighbor : neighbourhoud){
                if(currentNeighbor.solution.fitness < bestNeighbour.solution.fitness){
                    bestNeighbour = currentNeighbor;
                }
            }

            //Cas où on dégrade le fitness
            if(bestNeighbour.solution.fitness >= currentSol.fitness){
                //On ajoute la permutation interdite
                permutationInterdites.add(bestNeighbour.permutationDone);

                //Si la liste tabou est pleine, on supprime le premier élément
                if(permutationInterdites.size() > maxTabouSize){
                    permutationInterdites.remove(0);
                }

            }
            if(bestNeighbour.solution.fitness < bestSol.fitness){
                bestSol = new Solution(bestNeighbour.solution);
            }
            currentSol = bestNeighbour.solution;
            //ystem.out.println("Iteration " + i + " : " + currentSol.fitness+ " operator used : " + bestNeighbour.permutationDone.operateurUtilise);

        }
        Object[] result = new Object[2];
        result[0] = bestSol;
        result[1] = nbIterations;
        //System.out.println(nbIterations);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tabou tabou = (Tabou) o;
        return maxIterations == tabou.maxIterations && maxTabouSize == tabou.maxTabouSize && Objects.equals(permutationInterdites, tabou.permutationInterdites) && Objects.equals(etatsVisites, tabou.etatsVisites) && Objects.equals(initialSolution, tabou.initialSolution);
    }

    @Override
    public int hashCode() {
        return Objects.hash(permutationInterdites, etatsVisites, initialSolution, maxIterations, maxTabouSize);
    }
}

package projet.program.operators;

import projet.program.models.*;

import java.util.*;
import java.util.stream.Stream;

public class DeuxOpt {
    public Solution initialSolution;

    public DeuxOpt(Solution solution) {
        this.initialSolution = solution;
    }

    public Neighbor getNeighbor() {
        Random random = new Random();
        Solution bestSol = new Solution(initialSolution);
        double bestFitness = Double.POSITIVE_INFINITY;
        int nbIteration = 0;

        Solution currentSol = new Solution(initialSolution);
        List<Client> modifiedClients = new ArrayList<>();

        for(Route currenteRoute : currentSol.routes) {
            if(currenteRoute.route.size() <=3) {continue;}
            int indexStart = random.nextInt(1,currenteRoute.route.size()-2);
            int indexEnd = random.nextInt(indexStart+1,currenteRoute.route.size()-1);

            //On inverse les clients compris entre les deux indexs
            Route testRoute = new Route(currenteRoute.route);
            while(indexStart < indexEnd) {
                Client temp = testRoute.route.get(indexStart);
                modifiedClients.add(temp);
                testRoute.route.set(indexStart, testRoute.route.get(indexEnd));
                testRoute.route.set(indexEnd, temp);
                indexStart++;
                indexEnd--;
            }
            testRoute.calculateDistance();

            //Si la nouvelle route est valide on la remplace dans la solution
            if(testRoute.isValid()){
                nbIteration++;
                currentSol.routes.set(currentSol.routes.indexOf(currenteRoute), testRoute);
                currentSol.recalculate();
                if(currentSol.fitness < bestFitness) {
                    bestFitness = currentSol.fitness;
                    bestSol = new Solution(currentSol);
                    modifiedClients.clear();
                }
            }
            //Puisque on test une modification de route on reset la solution qu'on vient de modifier à partir de la solution initiale
            currentSol = new Solution(initialSolution);
        }

        Neighbor neighbor = new Neighbor(bestSol, new Permutation(modifiedClients, "2opt", nbIteration));
        return neighbor;

    }
}

package projet.program.operators;

import projet.program.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Move {
    public Solution initianSolution;

    public Move(Solution solution) {
        this.initianSolution = solution;
    }



    public Neighbor getNeighbor() {
        Solution bestSolution = new Solution(initianSolution);
        bestSolution.fitness = Double.POSITIVE_INFINITY;
        Solution currentSolution = new Solution(initianSolution);
        Client finalClientMoved = new Client();
        int nbIteration = 0;
        //On réalise 30 tests de move et on garde le meilleur
        for(int i = 0; i <30; i++) {
            Random random = new Random();
            //génération de l'index de la route et du client à déplacer
            int indexRoute = random.nextInt(currentSolution.routes.size());
            if(currentSolution.routes.get(indexRoute).route.size() <= 3) {
                continue;
            }
            int indexClient = random.nextInt(1, currentSolution.routes.get(indexRoute).route.size() - 1);

            ///on récupère le client à déplacer et le supprimons de sa route actuelle
            Client clientMoved = currentSolution.routes.get(indexRoute).route.get(indexClient);
            currentSolution.routes.get(indexRoute).route.remove(indexClient);

            //Création de la nouvelle route
            Route route = new Route();
            Client depot1 = currentSolution.routes.get(indexRoute).route.get(0);
            Client depot2 = currentSolution.routes.get(indexRoute).route.get(currentSolution.routes.get(indexRoute).route.size() - 1);
            route.route.add(depot1);
            route.route.add(clientMoved);
            route.route.add(depot2);
            currentSolution.routes.add(route);
            currentSolution.recalculate();
            nbIteration++;
            //Si la nouvelle route est meilleure que la bestSolution, on la garde
            if(currentSolution.fitness < bestSolution.fitness) {
                bestSolution = new Solution(currentSolution);
                finalClientMoved = clientMoved;
            }
            //On réinitialise notre current solution
            currentSolution = new Solution(initianSolution);
        }

        List<Client> permutations = new ArrayList<>();
        permutations.add(finalClientMoved);
        Neighbor neighbor = new Neighbor(bestSolution, new Permutation(permutations, "move", nbIteration));
        return neighbor;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return Objects.equals(initianSolution, move.initianSolution);
    }

    @Override
    public int hashCode() {
        return Objects.hash(initianSolution);
    }
}

package projet.program;


import projet.program.models.*;
import projet.program.operators.DeuxOpt;
import projet.program.operators.Exchange;
import projet.program.operators.Relocate;
import projet.program.solver.HillClimbing;
import projet.program.solver.Tabou;
import projet.program.solver.VRPTW;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;


public class Main {

    // Le jeu de données sur lequel travailler est à indiquer lors de l'appel de la fonction getFile("dataXXX")

    // Pour essayer l'une des parties de code, décommenter celle-ci

    // La fonction .output() inscrit une Solution dans le fichier "Data/ouput.txt", attention, cette fonction écrit à la suite du fichier, il est conseillé de vider celui-ci avant.

    public static void main(String[] args) {
//
//      //***************************************************
//      //      COMMENTAIRE 1 - Construction d'une Solution (initiale) optimisée

     /* VRPTW vrptw = new VRPTW();              // Initialisation de l'algo
      vrptw.getFile("data101");                        // Lecture du fichier
      Solution s0 = new Solution();

      while(!vrptw.allClientsVisited()){      // Tant que des clients ne sont pas visités
          ArrayList<Client> clients = vrptw.constructRoute();
          Route r = new Route(clients);
          s0.routes.add(r);
      }
      s0.recalculate();
      s0.output();       */       // resultats dans le fichier output.txt


        //***************************************************
        //      COMMENTAIRE 2 - Construction d'une Solution (initiale) aléatoire
      /*VRPTW vrptw = new VRPTW();              // Initialisation de l'algo
      vrptw.getFile("data101");                        // Lecture du fichier
      Solution sol = vrptw.createRandomSolution();
      sol.recalculate();
      sol.output(); */        // resultats dans le fichier output.txt

//      //***************************************************
//      //      COMMENTAIRE 3 - Affichage graphique d'un algorithme
      /*VRPTW vrptw = new VRPTW();              // Initialisation de l'algo
      vrptw.getFile("data101");                        // Lecture du fichier
      Solution s0 = new Solution();
      boolean surprise = false;          // Mettre a true pour un affichage un peu plus rigolo

      while(!vrptw.allClientsVisited()){      // Tant que des clients ne sont pas visités
          ArrayList<Client> clients = vrptw.constructRoute();
          Route r = new Route(clients);
          s0.routes.add(r);
      }
      Graph p = new Graph(s0);
      p.initialise();
      s0.recalculate();
      s0.output();              // resultats dans le fichier output.txt
      vrptw.bestSol = s0;
      vrptw.exchange(p, surprise);
      vrptw.relocate(p, surprise);
      vrptw.exchange(p, surprise);
      vrptw.relocate(p, surprise);*/


//      //***************************************************
//      //      COMMENTAIRE 4 - Calculs des taux d'amélioration des opérateurs
     /* ArrayList<Double> distancesInitiales = new ArrayList<>();
      ArrayList<Double> distances1iteration = new ArrayList<>();
      ArrayList<Double> distances2iteration = new ArrayList<>();
      ArrayList<Double> distances3iteration = new ArrayList<>();

      for (int i = 0; i < 200; i++) {
        System.out.println("Computing... " + i/2 +"% done.");
        VRPTW vrptw = new VRPTW();              // Initialisation de l'algo
        vrptw.getFile("data101");                        // Lecture du fichier
        Solution sol = vrptw.createRandomSolution();
        sol.recalculate();
        double distanceInitiale = sol.getTotSolDst();
        sol = new Relocate(sol).getNeighbor().getSolution();
        double distance1iteration = sol.getTotSolDst();
        sol = new Exchange(sol).getNeighbor().getSolution();
        double distance2iteration = sol.getTotSolDst();
        sol = new Relocate(sol).getNeighbor().getSolution();
        double distance3iteration = sol.getTotSolDst();
        distancesInitiales.add(distanceInitiale);
        distances1iteration.add(1 - (distance1iteration / distanceInitiale));
        distances2iteration.add(1 - (distance2iteration / distance1iteration));
        distances3iteration.add(1 - (distance3iteration / distance2iteration));
      }



      double sum1 = 0;
      double sum2 = 0;
      double sum3 = 0;
      for (double j : distances1iteration) {sum1 += j;}
      for (double k : distances2iteration) {sum2 += k;}
      for (double l : distances3iteration) {sum3 += l;}
      sum1 /= 2;
      sum2 /= 2;
      sum3 /= 2;

      System.out.println(sum1);
      System.out.println(sum2);
      System.out.println(sum3); */


        //      //***************************************************
//      //      COMMENTAIRE 5 - Calculs du nombre de camions
     /* VRPTW vrptw = new VRPTW();              // Initialisation de l'algo
      vrptw.getFile("data101");                        // Lecture du fichier
      Solution s0 = new Solution();
      while(!vrptw.allClientsVisited()){      // Tant que des clients ne sont pas visités
          ArrayList<Client> clients = vrptw.constructRoute();
          Route r = new Route(clients);
          s0.routes.add(r);
      }
      s0.recalculate();

      int solOpti = s0.routes.size();

      Relocate r = new Relocate(s0);
      s0 = r.getNeighbor().getSolution();
      Exchange e = new Exchange(s0);
      s0 = e.getNeighbor().getSolution();
      Relocate r2 = new Relocate(s0);
      s0 = r2.getNeighbor().getSolution();
      Exchange e2 = new Exchange(s0);
      s0 = e2.getNeighbor().getSolution();

      Double random = 0.0;
      Double randomRER = 0.0;
      Double randomERE = 0.0;

      for (int i = 0; i < 100; i++) {
          System.out.println(i);
          VRPTW vrptw2 = new VRPTW();              // Initialisation de l'algo
          vrptw2.getFile("data101");                        // Lecture du fichier
          Solution sol = vrptw2.createRandomSolution();
          sol.recalculate();
          random += sol.routes.size();
          Solution sol2 = new Relocate(sol).getNeighbor().getSolution();
          sol2 = new Exchange(sol2).getNeighbor().getSolution();
          sol2 = new Relocate(sol2).getNeighbor().getSolution();
          randomRER += sol2.routes.size();

          VRPTW vrptw3 = new VRPTW();              // Initialisation de l'algo
          vrptw3.getFile("data101");                        // Lecture du fichier
          Solution solBis = vrptw3.createRandomSolution();
          solBis.recalculate();
          Solution sol3 = new Relocate(solBis).getNeighbor().getSolution();
          sol3 = new Exchange(sol3).getNeighbor().getSolution();
          sol3 = new Relocate(sol3).getNeighbor().getSolution();
          randomERE += sol3.routes.size();
      }
      System.out.println("Solution intelligente : " + solOpti);
      System.out.println("Solution intelligente optimisee : " + s0.routes.size());
      System.out.println("Solution aleatoire : " + random/100);
      System.out.println("Solution aleatoire RER : " + randomRER/100);
      System.out.println("Solution aleatoire ERE : " + randomERE/100); */


        //***************************************************
        //      COMMENTAIRE 6 - Calculs du temps d'execution
      /*long startTime = System.nanoTime();
      for (int i = 0; i < 200; i++) {
          VRPTW vrptw = new VRPTW();              // Initialisation de l'algo
          vrptw.getFile("data101");                        // Lecture du fichier
          Solution s0 = vrptw.createRandomSolution();
          s0.recalculate();
          s0 = new Exchange(s0).getNeighbor().getSolution();        // On peut remplacer exchange par Relocate ou Ajouter d'autres operateurs
      }
      long endTime = System.nanoTime();
      long executionTime = (endTime - startTime) / 200000000;
      System.out.println("Temps d'exécution : " + executionTime + " ms"); */


//      //***************************************************
//      //      COMMENTAIRE 7 - Amelioration de la Solution en fonction du temps
        /*ArrayList<Long> time1  = new ArrayList<>();
        ArrayList<Double> dist1 = new ArrayList<>();
        ArrayList<Long> time2  = new ArrayList<>();
        ArrayList<Double> dist2 = new ArrayList<>();


        for (int i = 0; i < 200; i++){
            System.out.println("Computing..." + i/2 + "%");
            VRPTW vrptw = new VRPTW();              // Initialisation de l'algo
            vrptw.getFile("data101");                        // Lecture du fichier)
            Solution sol = vrptw.createRandomSolution();
            sol.recalculate();
            Relocate r = new Relocate(sol);
            Exchange e = new Exchange(sol);
            double distInitiale = sol.fitness;

            long startTime = System.nanoTime();

            sol = r.getNeighbor().getSolution();
            long checkTime1 = System.nanoTime();
            time1.add(checkTime1 - startTime);
            dist1.add(sol.fitness / distInitiale * 100);

            sol = e.getNeighbor().getSolution();
            long checkTime2 = System.nanoTime();
            time2.add(checkTime2 - startTime);
            dist2.add(sol.fitness / distInitiale * 100);

        }

        long sum = 0;
        for (long value : time1) {sum += value;}
        double average = (double) sum / time1.size();
        double sumBis = 0;
        for (double valueBis : dist1) {sumBis += valueBis;}
        double averageBis = 100 - (sumBis / dist1.size());

        long sum2 = 0;
        for (long value2 : time2) {sum2 += value2;}
        double average2 = (double) sum2 / time2.size();
        double sumBis2 = 0;
        for (double valueBis2 : dist2) {sumBis2 += valueBis2;}
        double averageBis2 = 100 - (sumBis2 / dist2.size());


        System.out.println("Iteration 1 Average : " + average + " % to initial " + averageBis);
        System.out.println("Iteration 2 Average : " + average2 + " % to initial " + averageBis2);*/

        //***************************************************
        //      COMMENTAIRE 8 - Test de la méthode Tabou sour l'ensemble des jeux de données
        //      Pour faciliter les tests nous utilisons des threads. De la sorte nous diminuons drastiquement le temps d'execution de la boucle
        //***************************************************
        /*ArrayList<String> files = new ArrayList<>();
        files.add("data101");
        files.add("data102");
        files.add("data111");
        files.add("data112");
        files.add("data201");
        files.add("data202");
        files.add("data1101");
        files.add("data1102");
        files.add("data1201");
        files.add("data1202");
        for(String file : files) {
                long startTime = System.currentTimeMillis();
                VRPTW vrptw = new VRPTW();  // Initialisation de l'algo
                vrptw.getFile("data1202");        // Lecture du fichier
                int nbrIterations = 300;
                AtomicReference<Double> bestFitness = new AtomicReference<>(Double.POSITIVE_INFINITY);
                AtomicReference<Double> avrgFitness = new AtomicReference<>(0.0);
                AtomicReference<Solution> bestSolOverall = new AtomicReference<>(null);
                AtomicReference<Integer> iteration = new AtomicReference<>(0);

                // Création d'un tableau pour stocker les threads
                Thread[] threads = new Thread[nbrIterations];

                for (int i = 0; i < nbrIterations; i++) {
                    threads[i] = new Thread(() -> {
                        Solution sol = vrptw.createRandomSolution();
                        sol.recalculate();
                        Tabou tabou = new Tabou(130, 45, sol);
                        Object[] res = tabou.solve();
                        Solution bestSol = (Solution) res[0];
                        int nbiteration = (int) res[1];

                        //System.out.println("Solution initiale (itération " + iteration + "): " + sol.fitness);
                        //System.out.println("Solution finale (itération " + iteration + "): " + bestSol.fitness);

                        synchronized (vrptw) {
                            avrgFitness.set(avrgFitness.get() + bestSol.fitness);
                            iteration.set(iteration.get() + nbiteration );
                            if (bestSol.fitness < bestFitness.get()) {
                                bestFitness.set(bestSol.fitness);
                                bestSolOverall.set(bestSol);
                            }
                        }
                    });

                    threads[i].start();
                }

                // Attendre la fin de tous les threads

                for (int i = 0; i < nbrIterations; i++) {
                    try {
                        threads[i].join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                long endTime = System.currentTimeMillis();
                long executionTime = (endTime - startTime)/1000;
                System.out.println("Temps d'exécution : " + executionTime + " ms");
                System.out.println("Best fitness: " + bestFitness.get());
                System.out.println("Average fitness: " + avrgFitness.get() / nbrIterations);
                System.out.println("Average solution created: " + iteration.get() / nbrIterations);
                bestSolOverall.get().output();
        } */

        // resultats dans le fichier output.txt


        //***************************************************
        //      COMMENTAIRE 9 - Calculs du nombre de solutions créées


        /*int essais = 140;
        int totalHC_2O = 0;
        int totalHC_3O = 0;
        int totalHC_4O = 0;
        int totalTabou = 0;

        for (int i = 0; i < essais; i++) {
            System.out.println(i);
            VRPTW vrptw = new VRPTW();  // Initialisation de l'algo
            vrptw.getFile("data1202");
            int nbrSolHC_2Operateurs = 0;
            int nbrSolHC_3Operateurs = 0;
            int nbrSolHC_4Operateurs = 0;
            Solution solHC = vrptw.createRandomSolution();
            solHC.recalculate();
            Neighbor n = new Relocate(solHC).getNeighbor();
            nbrSolHC_2Operateurs += n.permutationDone.nbIteration;
            nbrSolHC_3Operateurs += n.permutationDone.nbIteration;
            nbrSolHC_4Operateurs += n.permutationDone.nbIteration;
            solHC = n.getSolution();
            Neighbor n2 = new Exchange(solHC).getNeighbor();
            nbrSolHC_2Operateurs += n2.permutationDone.nbIteration;
            nbrSolHC_3Operateurs += n2.permutationDone.nbIteration;
            nbrSolHC_4Operateurs += n2.permutationDone.nbIteration;
            solHC = n2.getSolution();
            Neighbor n3 = new Relocate(solHC).getNeighbor();
            nbrSolHC_3Operateurs += n3.permutationDone.nbIteration;
            nbrSolHC_4Operateurs += n3.permutationDone.nbIteration;
            solHC = n3.getSolution();
            Neighbor n4 = new Exchange(solHC).getNeighbor();
            nbrSolHC_4Operateurs += n4.permutationDone.nbIteration;

            totalHC_2O += nbrSolHC_2Operateurs;
            totalHC_3O += nbrSolHC_3Operateurs;
            totalHC_4O += nbrSolHC_4Operateurs;

            Solution solTabou = vrptw.createRandomSolution();
            solTabou.recalculate();
            Tabou tabou = new Tabou(130, 10, solTabou);
            Object[] res = tabou.solve();
            totalTabou += (int) res[1];

        }
        totalHC_2O /= essais;
        totalHC_3O /= essais;
        totalHC_4O /= essais;
        totalTabou /= essais;
        System.out.println("HC 2O : " + totalHC_2O);
        System.out.println("HC 3O : " + totalHC_3O);
        System.out.println("HC 4O : " + totalHC_4O);
        System.out.println("Tabou : " + totalTabou);*/

        System.out.println("Program completed");


    }
}

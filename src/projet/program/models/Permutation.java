package projet.program.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Permutation {
    public List<Client> clientsModifies;

    public String operateurUtilise;

    public int nbIteration;

    public Permutation(List<Client> clientsModifies, String operateurUtilis, int nbIteration) {
        this.clientsModifies = clientsModifies;
        this.operateurUtilise = operateurUtilise;
        this.nbIteration = nbIteration;

    }

    public Permutation() {
        this.clientsModifies = new ArrayList<>();
        this.operateurUtilise = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permutation that = (Permutation) o;
        return Objects.equals(clientsModifies, that.clientsModifies) && Objects.equals(operateurUtilise, that.operateurUtilise);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientsModifies, operateurUtilise);
    }
}

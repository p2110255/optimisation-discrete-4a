package projet.program.models;

import projet.program.models.Route;
import projet.program.models.Solution;

import java.awt.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;


public class Graph extends JPanel {


    private final List<Color> couleurs;

    private Solution currentSol;

    public Graph(Solution solution) {
        this.couleurs = Arrays.asList(
                new Color(0,180,255), //Very light blue
                new Color(0,0,230),//Dark Blue
                new Color(0,152,0),//Dark Green
                new Color(255,180,0),//Gold
                new Color(255,0,0),//Orange
                new Color(200, 0, 208),  // Violet
                new Color(135,136,136)
                //Color.RED,
        );
        this.currentSol = solution;
        setPreferredSize(new Dimension(1000, 1000));
    }

    public void initialise(){
        JFrame frame = new JFrame("Graphique avec chemins");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(this);
        frame.pack();
        frame.setVisible(true);
    }

    public void changeSolution(Solution solution, boolean playSound) {
        this.currentSol = solution;
        this.initialise();
        if (playSound) {
            this.play("Data/Duck.wav");
        }
        repaint(); // redessine le graphique avec les nouveaux chemins
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int scale = 12;
        int j = 0;
        // Dessin de chaque chemin avec une couleur différente
        for(Route currentChemin : currentSol.routes){


            //On choisi la couleur qui va représenter le chemin
            Color couleur = couleurs.get(j% couleurs.size());
            currentChemin.calculateDistance();

            //on dessine chaque sommet et les liaisons

            for(int i = 0; i < currentChemin.route.size() -1; i++){
                //On récupère les cooords du currentClient
                int x1 = currentChemin.route.get(i).x*scale;
                int y1 = currentChemin.route.get(i).y*scale;
                //Si ce n'est pas le dernier client de la route on le relie au prochain Client
                int x2 = currentChemin.route.get(i+1).x*scale;
                int y2 = currentChemin.route.get(i+1).y*scale;
                //Si on est au milieu du chemin on affiche le nom du chemin
                if(i == (currentChemin.route.size()-1)/2){
                    g.setColor(Color.BLACK);
                    g.drawString("Route : "+currentSol.routes.indexOf(currentChemin)+"\n\n Tt Dist :"+(int)(currentChemin.totalRouteDistance), (x1 +x2)/2 , (y1+y2)/2);

                }
                g.setColor(couleur);
                g.drawLine(x1, y1, x2, y2);
                g.setColor(Color.BLACK);
                if(i==0){g.setColor(Color.RED);}
                g.drawString(""+i,x1-10, y1-10);
                g.fillOval(x1 -6 , y1 -6 , 12, 12);
            }
            j+=3;
        }
    }

    public void play(String filePath) {

        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(filePath));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception e) {
            System.out.println("Error playing audio file: " + e.getMessage());
        }

    }
}
